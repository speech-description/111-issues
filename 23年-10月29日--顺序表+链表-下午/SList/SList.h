#pragma once
#include<stdio.h>
#include<stdlib.h>

typedef int SLNDataType;

// Single List
typedef struct SListNode
{
	SLNDataType val;
	struct SListNode* next;
}SLNode;

void SLTPrint(SLNode* phead);
void SLTPushBack(SLNode* phead, SLNDataType x);
void SLTPushFront(SLNode* phead, SLNDataType x);

