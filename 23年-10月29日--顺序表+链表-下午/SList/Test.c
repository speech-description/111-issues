#include"SList.h"

int main()
{
	SLNode* plist = NULL;
	SLTPushBack(plist, 1);
	SLTPushBack(plist, 2);
	SLTPushBack(plist, 3);
	SLTPushBack(plist, 4);

	SLTPrint(plist);

	return 0;
}

//void Swap(int* p1, int* p2)
//{
//	int tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//
//// 改变的是int，传的是int*
//int main()
//{
//	int a = 0, b = 1;
//	Swap(&a, &b);
//
//	return 0;
//}

//void Swap(int** p1, int** p2)
//{
//	int tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//
//// 改变的是int*，传的是int**
//int main()
//{
//	int* px = NULL, * py = 0x01;
//	Swap(&px, &py);
//
//	return 0;
//}